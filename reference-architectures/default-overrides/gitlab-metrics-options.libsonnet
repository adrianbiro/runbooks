// For more information on how to configure options, consult
// https://gitlab.com/gitlab-com/runbooks/-/blob/master/reference-architectures/README.md.
{
  elasticacheMonitoring: false,
  rdsMonitoring: false,
  rdsInstanceRAMBytes: null,
  rdsMaxAllocatedStorageGB: null,
}
